﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControllerScript : MonoBehaviour
{
    public class Movement
    {
        private bool _done;
        private Quaternion _rotation;

        public Quaternion Rotation
        {
            get {
                _done = true;
                return _rotation; 
            }
            set {
                _rotation = value;
                _done = false;
            }
        }

        public bool hasNext()
        {
            return !_done;
        }

    }

    private Movement _movement;
    private Quaternion _originalWorldPlaneRotation;

    [SerializeField]
    private Transform _worldPlane;

    [SerializeField]
    private float _rotationSpeed = 10f;

    private float _rotationAngleUnit = 90f;

    [SerializeField]
    public float _screenHeight;
    [SerializeField]
    public float _screenWidth;

    private float _heightMultiplier;
    private float _widthMultiplier;

    public GameControllerScript()
    {
        _movement = new Movement();
    }

    private void Awake()
    {
        _originalWorldPlaneRotation = _worldPlane.rotation;
    }

    private void Start()
    {
        _screenHeight = Screen.height * 2f;
        _screenWidth = Screen.width * 2f;
        _heightMultiplier = 2f / _screenHeight;
        _widthMultiplier = 2f / _screenWidth;
    }

    private void Update()
    {
        Vector3 dir = Vector3.zero;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            dir.x = touch.position.y * _widthMultiplier - 1;
            dir.z = -touch.position.x * _heightMultiplier + 1;

            Debug.Log("(" + touch.position.x + ", " + touch.position.y + ")");

            if (dir.sqrMagnitude > 1)
            {
                dir.Normalize();
            }
            
            dir = dir * _rotationAngleUnit;
            _movement.Rotation = _originalWorldPlaneRotation * Quaternion.Euler(dir.x, dir.y, dir.z);
            return;
        }

       
        dir.x = Input.acceleration.y;
        dir.z = -Input.acceleration.x;
        if (dir.sqrMagnitude > 1) {
            dir.Normalize();
        }
        dir = dir * _rotationAngleUnit;
        _movement.Rotation = _originalWorldPlaneRotation * Quaternion.Euler(dir.x, dir.y, dir.z);
    }

    private void FixedUpdate()
    {
        if (_movement.hasNext())
        {
            Quaternion rotation = _movement.Rotation;
            _worldPlane.rotation = Quaternion.Lerp(_worldPlane.rotation, rotation, _rotationSpeed * Time.fixedDeltaTime);
        }
    }
}
