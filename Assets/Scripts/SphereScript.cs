﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereScript : MonoBehaviour
{
    private const string _edibleTag = "Edible";

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(_edibleTag))
        {
            collision.collider.transform.parent = this.transform;
        }
    }

}
